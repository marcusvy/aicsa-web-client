import { AicsaPage } from './app.po';

describe('aicsa App', function() {
  let page: AicsaPage;

  beforeEach(() => {
    page = new AicsaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
