import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UiComponent } from './ui.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import { MenuComponent } from './menu/menu.component';
import { MenuSwitchComponent } from './menu/menu-switch/menu-switch.component';
import { MenuService } from './menu/menu.service';

@NgModule({
  declarations: [
    UiComponent,
    HeaderComponent,
    FooterComponent,
    ButtonComponent,
    InputComponent,
    MenuComponent,
    MenuSwitchComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ButtonComponent,
    InputComponent,
    MenuComponent,
    MenuSwitchComponent
  ],
  providers: [MenuService],
  bootstrap: [UiComponent]
})
export class UiModule { }
