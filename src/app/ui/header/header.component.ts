import { Component, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'mv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() image: string = '';
  @Input() title: string = '';
  @Input() subtitle: string = '';
  @Input() button: string = '';
  @Input() buttonEvent: string;
  @Input() filter: Boolean = false;
  @Input() noLogo: Boolean = false;

  constructor() { }

  ngOnInit() {
    if( this.image.length===0 ){
      this.filter = false;
    }
  }
  hasContent() {
    return ( this.title.length > 0) ||
           ( this.subtitle.length > 0) ||
           ( this.button.length > 0);
  }

  hasImage() {
    return (this.image.length > 0);
  }

  getHeaderClass() {
    let classes = {
      'mv-header--with-image' : this.hasImage(),
      'mv-header--no-logo' : this.noLogo
    };
    return classes;
  }

  getHeaderStyle() {
    let style = {
      'background-image': `url('${this.image}')`
    }
    return style;
  }

}
