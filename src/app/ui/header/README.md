# mv-header
------------------------

Painel superior com suporte a menu

# Uso

```html
<mv-header title="Consultoria Empresarial"
  subtitle="Podemos marcar um cafezinho"
  image="/assets/img/features/predio.jpg"
  button="Contato">
  <img src="/assets/img/mvinicius_logo.svg" alt="" />
</mv-header>
```

# Propriedades

* title : (string) : Título
* subtitle : (string) : Segunda linha de título
* image : (string) : Imagem de plano de fundo
* button : (string) : Botão de ação
* buttonEvent : (string) : Evento para o botão de ação
