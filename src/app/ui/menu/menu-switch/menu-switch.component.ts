import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MenuService } from './../menu.service';

@Component({
  selector: 'mv-menu-switch',
  templateUrl: './menu-switch.component.html',
  styleUrls: ['./menu-switch.component.scss']
})
export class MenuSwitchComponent implements OnInit {

  private page: string = '';

  constructor(private menuService: MenuService) {}

  ngOnInit() {

  }
  onMenuToggle() {
    this.menuService.toggle();
  }
}
