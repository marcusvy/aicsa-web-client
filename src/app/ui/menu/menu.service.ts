import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class MenuService {

  private open: Boolean = false;
  private statusDataSource: Subject<Boolean> = new BehaviorSubject<Boolean>(false); //dados
  status$ = this.statusDataSource.asObservable(); //stream

  constructor() { }

  /**
   * Toggle menu
   * @return {Boolean} The status result
   */
  toggle() {
    this.open = !this.open;
    this.statusDataSource.next(this.open);
    return this.open;
  }

  close () {
    this.open = false;
    this.statusDataSource.next(this.open);
  }

  get() {
    return this.open;
  }

}
