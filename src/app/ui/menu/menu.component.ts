import { Component, OnInit, OnDestroy, HostBinding, ViewEncapsulation} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from './menu.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'mv-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuComponent implements OnInit, OnDestroy {

  open: Boolean;
  subscription: Subscription;
  @HostBinding('class.mv-menu--open') classOpen: Boolean = this.open;

  constructor(
    private menuService: MenuService,
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        if(this.open){
          this.onCloseMenu();
        }
      }
    });

    this.subscription = this.menuService.status$.subscribe(
      open => {
        this.open = open;
        this.classOpen = open;
      }
    );
  }

  onCloseMenu() {
    this.menuService.close();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
